module.exports = (sequelize,DataTypes) => {
    return sequelize.define(
        "Todo",
        {
          id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
          },
          text: {
            type: DataTypes.STRING,
            // allowNull defaults to true
          },
          isCompleted: {
            type: DataTypes.BOOLEAN,
            // allowNull defaults to true
          },
        },
        {

          
          createdAt: false,
          updatedAt: false
        }
      );
}
  