const { Sequelize, DataTypes } = require("sequelize");
require('dotenv').config()
const env =process.env;

const sequelize = new Sequelize(
  `postgres://${env.dbUsername}:${env.dbPassword}@${env.dbHost}:${env.dbPort}/${env.dbName}`
);

const Todo = require('./schema')(sequelize,DataTypes) 

async function main() {
    await sequelize.authenticate();
    await Todo.sync({ force: true });
    await Todo.bulkCreate([
        {
          "text": "Buy groceries",
          "isCompleted": true
        },
        {
          "text": "Call mom",
          "isCompleted": false
        },
        {
          "text": "Finish work report",
          "isCompleted": true
        },
        {
          "text": "Go for a run",
          "isCompleted": false
        },
        {
          "text": "Read a book",
          "isCompleted": true
        },
        {
          "text": "Clean the house",
          "isCompleted": false
        },
        {
          "text": "Attend a meeting",
          "isCompleted": true
        },
        {
          "text": "Plan a trip",
          "isCompleted": false
        },
        {
          "text": "Watch a movie",
          "isCompleted": true
        },
        {
          "text": "Learn a new language",
          "isCompleted": false
        }
      ]
      );
}
main();

module.exports=Todo;