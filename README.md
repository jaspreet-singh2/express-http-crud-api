# Node.js Todo API

This is a simple Node.js API for managing Todo items using the Express.js framework and Sequelize ORM for PostgreSQL.

## Directory Structure

|── midelware
│ └── Validation.js
├── Routes
│ └── routes.js
├── Models
│ ├── schema.js
│ └── sequelize.js
├── server.js
├── package.json
└── package-lock.json


### Files Explanation

1. **server.js**:
   - The main entry point of the Node.js application.
   - Initializes the Express.js server and sets up API routing.
   - Listens on port 3000 for incoming HTTP requests.

2. **midelware/Validation.js**:
   - Defines middleware for request body validation using the `yup` library.
   - Exports an async middleware function that validates the request body against a    predefined schema.
   - Sends a 400 Bad Request response with an error message if validation fails.

3. **Routes/routes.js**:
   - Defines route handlers for API endpoints.
   - Includes functions for:
     - Getting all todos (`getAllTodos`).
     - Deleting a todo by ID (`deleteTodoById`).
     - Updating a todo by ID (`updateTodoById`).
     - Creating a new todo (`createTodo`).
     - Getting a todo by ID (`getTodoById`).
   - Each function handles HTTP requests and interacts with the database as needed.

4. **Models/schema.js**:
   - Defines the schema for the "Todo" model using Sequelize, an ORM.
   - Specifies the structure of a "Todo" item, including fields like "id," "text," and "isCompleted."

5. **Models/sequelize.js**:
   - Sets up the Sequelize instance and initializes the "Todo" model.
   - Connects to a PostgreSQL database using environment variables defined in the `.env` file.
   - The `main()` function:
     - Authenticates with the database.
     - Synchronizes the "Todo" model with the database, creating the table if it doesn't exist.
     - Populates initial data into the "Todo" table using `bulkCreate`.
   - Exports the `Todo` model for use in other parts of the application.

6. **.env** (Not provided in the directory structure but required):
   - Contains sensitive configuration information such as database credentials.
   - Configures environment variables for the application.
   - Not included in the repository to protect sensitive data.

## Getting Started

Follow these steps to install and run the server:

1. Clone the repository:


2. Install dependencies:

`npm install`

3. Configure environment variables by creating a `.env` file with your database credentials.

4. Start the server:

`node server.js


Your server should now be running on port 3000, and you can use the provided API endpoints for managing Todo items.

## API Endpoints

- Get all todos: `GET http://localhost:3000/todos`
- Get a todo by ID: `GET http://localhost:3000/todos/:id`
- Create a new todo: `POST http://localhost:3000/todos`
- Update a todo by ID: `PUT http://localhost:3000/todos/:id`
- Delete a todo by ID: `DELETE http://localhost:3000/todos/:id`



