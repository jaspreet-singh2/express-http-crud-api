const express = require("express");
const app = express();
const  bodyParser = require("body-parser");
const { getAllTodos, deleteTodoById, updateTodoById, createTodo,getTodoById } = require('./Routes/routes')
const validator=require('./middleware/Validation')


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.get('/todos', getAllTodos);
app.delete('/todos/:id', deleteTodoById);
app.put('/todos/:id',validator, updateTodoById);
app.post('/todos',validator, createTodo);
app.get('/todos/:id', getTodoById);


app.listen(3000);
