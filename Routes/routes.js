const Todo = require("../Models/sequelize");

// Function to get all todos
const getAllTodos = async (req, res) => {
  try {
    const todos = await Todo.findAll();
    res.status(200).json(todos);
  } catch (err) {
    res.status(500).json({ error: 'Internal server error' });
  }
};

// Function to delete a todo by ID
const deleteTodoById = async (req, res) => {
  try {
    const result = await Todo.destroy({ where: { id: req.params.id } });
    if (result === 0) {
      res.status(404).json({ message: 'Todo not found' });
    } else {
      res.status(204).json({ message: 'Todo was  Deleted' })
    }
  } catch (err) {
    res.status(500).json({ error: 'Internal server error' });
  }
};

// Function to update a todo by ID
const updateTodoById = async (req, res) => {
  try {
    await Todo.update(req.body, { where: { id: req.params.id } });
    const updatedTodo = await Todo.findByPk(Number(req.params.id));
    res.status(200).json(updatedTodo);
  } catch (err) {
    res.status(500).json({ error: 'Internal server error' });
  }
};

// Function to create a new todo
const createTodo = async (req, res) => {
  try {
    const createdRow = await Todo.create(req.body);
    res.status(201).json(createdRow);
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
};

// Function to get a todo by ID
const getTodoById = async (req, res) => {
  try {
    const todo = await Todo.findByPk(Number(req.params.id));
    if (todo !== null) {
      res.status(200).json(todo);
    } else {
      res.status(404).json({ message: 'Todo not found' });
    }
  } catch (err) {
    res.status(500).json({ error: 'Internal server error' });
  }
};

module.exports = {
  getAllTodos,
  deleteTodoById,
  updateTodoById,
  createTodo,
  getTodoById
};
