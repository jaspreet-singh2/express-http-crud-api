const { object, string, boolean } = require("yup");

let Todoschema = object({ text: string(), isCompleted: boolean() });



module.exports =  async (req, res, next) => {
    try {
      // Validate the request body against the schema
      await Todoschema.validate(req.body);
      next();
    } catch (error) {
      // If validation fails, send an error response
      res.status(400).json({ error: error.message });
    }
  };